# Subdued Universal Uncrafter

## Description

Introduces a more subdued black/white with orange spark version of the Universal Uncrafter.

It introduces some balance changes on its recipe:
- 3x Tungsten Bar
- 5x Silver Bar
- 3x Gold Bar
- 5x Silicon Board
- 300 Pixel

This change stops the abuse of Tier 1 RNG weapon uncrafting and slows down progression a bit.
Additionally, forces the player to farm Silicon Board in Space Encounters or wait until the Atomic Furnace to craft them.

## Compatibility

- Compatible with Starbound 1.4
- Compatible with Frackin' Universe
- *Incompatible with the original Universal Uncrafter*


## Steam Workshop Links

- Original Universal Uncrafter: https://steamcommunity.com/sharedfiles/filedetails/?id=729532886
- Subdued Universal Uncrafter: https://steamcommunity.com/sharedfiles/filedetails/?id=1994397474

## Credits

- All credit to its original author: Peasly Wellbott.